# permeability reduction

Matlab scripts to simulate the reduced permeability of porous granular media due to ice or hydrate. The functions are organized in two classes, including:
- `soil.m`. Calculate the soil freezing curves using a Monte Carlo method on synthetic soil models.
- `frw.m`. Calculate the permeabilities of the synthetic soil models, ice-free or ice-bearing, and compare the results against published models and measurements.
- `duct.m`. Examples showing the calculations in straight ducts of simple cross-sections.

Parameters and data for calculations are in:
- `param.json`. All relevant parameters.
- `data/conductivity.xlsx`. Digitized measurements used for validation.
- `data/kr_finney.mat`. Simulated permeability reduction of Finney pack.
- `data/mc`. Simulated Monte Carlo results to calculate the soil freezing curves.
- `data/deposit`. Synthetic soil models, including the mono-dispersed Finney pack, and poly-dispersed synthetic soil.

This repository was published in
*Estimating Permeability of Partially Frozen Soil Using Floating Random Walks*, Water Resources Research 57 (12).
