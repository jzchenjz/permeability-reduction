classdef duct
    % Use floating random walk method to approximate the effective permeability
    % of ducts of arbitary cross-sections.
    % Simple tests are included for ducts of equilateral triangular and
    % rectangular cross-sections.

    methods (Static)

        % ----- Simple geometry ----- %

        function k = rectangular(a, b)
            % Calculate the permeability of a rectangular tube with
            % two sides a and b. The two inputs are symmetric.

            if nargin < 2
                b = a;
            end

            x = pi * a / 2 / b;
            y = 16 * b^3 / pi^5 / a;

            z = 0;
            n = 1;
            delta = 1 / (2 * n - 1)^5 * tanh((2 * n - 1) * x);

            while delta > 1e-8
                z = z + delta;
                n = n + 1;
                delta = 1 / (2 * n - 1)^5 * tanh((2 * n - 1) * x);
            end

            k = b^2/12 - y * z;
        end

        function points = sample(pgon, N)
            % generate N random points within the polygon pgon.

            xv = pgon.Vertices(:, 1);
            yv = pgon.Vertices(:, 2);

            xspan = range(xv);
            yspan = range(yv);
            Xmin = min(xv);
            Ymin = min(yv);
            P = floor(N / area(pgon) * xspan * yspan * 2);
            xrand = Xmin + rand([P, 1]) * xspan;
            yrand = Ymin + rand([P, 1]) * yspan;
            in = inpolygon(xrand, yrand, xv, yv);
            points = [xrand(in), yrand(in)];
            idx = randperm(sum(in));
            points = points(idx, :);
            points = points(1:N, :);
        end

        function [d, x_poly, y_poly] = distance(p, pgon)
            % Find minimum distance d from a point p = (x, y) to the
            % polygon pgon.
            % Output:
            % d - distance from point to polygon (defined as a minimal distance
            % from point to any of polygon's ribs, negative if the point is
            % outside the polygon and positive otherwise)
            % x_poly: x coordinate of the point in the polygon closest to x, y
            % y_poly: y coordinate of the point in the polygon closest to x, y
            % mathworks.com/matlabcentral/fileexchange/12744
            % Revision history:
            % 03/01/2020 - change the negative sign because all I need is point
            % inside the polygon
            % - Changes by Arthur Chen
            % 03/31/2008 - return the point of the polygon closest to x, y
            % - added the test for the case where a polygon rib is either
            % horizontal or vertical. From Eric Schmitz.
            % - Changes by Alejandro Weinstein
            % 07/09/2006 - case when all projections are outside of polygon ribs
            % 23/05/2004 - created by Michael Yoshpe

            xv = pgon.Vertices(:, 1);
            yv = pgon.Vertices(:, 2);

            % rho = duct.polygon_dist(p(1), p(2), xv, yv);

            x = p(1);
            y = p(2);
            Nv = numel(xv);

            if ((xv(1) ~= xv(Nv)) || (yv(1) ~= yv(Nv)))
                xv = [xv; xv(1)];
                yv = [yv; yv(1)];
                % Nv = Nv + 1;
            end

            % linear parameters of segments that connect the vertices
            % Ax + By + C = 0
            A =- diff(yv);
            B = diff(xv);
            C = yv(2:end) .* xv(1:end - 1) - xv(2:end) .* yv(1:end - 1);

            % find the projection of point (x, y) on each rib
            AB = 1 ./ (A.^2 + B.^2);
            vv = (A * x + B * y + C);
            xp = x - (A .* AB) .* vv;
            yp = y - (B .* AB) .* vv;

            % test where a polygon rib is either horizontal or vertical.
            % from Eric Schmitz
            id = find(diff(xv) == 0);
            xp(id) = xv(id);
            id = find(diff(yv) == 0);
            yp(id) = yv(id);

            % find all cases where projected point is inside the segment
            idx_x = (((xp >= xv(1:end - 1)) & (xp <= xv(2:end))) ...
            | ((xp >= xv(2:end)) & (xp <= xv(1:end - 1))));
            idx_y = (((yp >= yv(1:end - 1)) & (yp <= yv(2:end))) ...
                | ((yp >= yv(2:end)) & (yp <= yv(1:end - 1))));
            idx = idx_x & idx_y;

            % distance from point (x, y) to the vertices
            dv = sqrt((xv(1:end - 1) - x).^2 + (yv(1:end - 1) - y).^2);

            if (~any(idx))
                % all projections are outside of polygon ribs
                [d, I] = min(dv);
                x_poly = xv(I);
                y_poly = yv(I);
            else
                % distance from point (x, y) to the projection on ribs
                dp = sqrt((xp(idx) - x).^2 + (yp(idx) - y).^2);
                [min_dv, I1] = min(dv);
                [min_dp, I2] = min(dp);
                [d, I] = min([min_dv min_dp]);

                if I == 1
                    % the closest point is one of the vertices
                    x_poly = xv(I1);
                    y_poly = yv(I1);
                elseif I == 2
                    % the closest point is one of the projections
                    idxs = find(idx);
                    x_poly = xp(idxs(I2));
                    y_poly = yp(idxs(I2));
                end

            end

            if ~inpolygon(x, y, xv, yv)
                d =- d;
            end

        end

        % ----- Equilateral triangle and square ducts ----- %

        function pgon = equilateral(a)
            % Generate a equilateral triangle of a side a.

            pgon = polyshape([0, a, a / 2], [0, 0, a / 2 * sqrt(3)]);

        end

        function pgon = rectangle(a, b)
            % Create a rectangle of two sides a and b.

            if nargin < 2
                b = a;
            end

            pgon = polyshape([0, a, a, 0], [0, 0, b, b]);

        end

        % ----- Floating random-walk ----- %

        function y = floating(pxy, pgon, tau, M)
            % Initiate floating random walk in the polygon duct
            % starting from random points pxy, with tolerance tau.

            close all
            % number of points
            N = size(pxy, 1);
            y = zeros(N, 1); % array of random walk tallies

            parfor i = 1:N

                p = pxy(i, :);

                for j = 1:M
                    pn = p;
                    rho = duct.distance(pn, pgon);

                    y(i) = y(i) + rho^2;

                    while rho > tau
                        theta = 2 * pi * rand;
                        pn = pn + rho * [cos(theta), sin(theta)];
                        rho = duct.distance(pn, pgon);
                        y(i) = y(i) + rho^2;
                    end

                end

            end

        end

        function keff = darcy(pgon)
            % Calculate the effective permeability for a duct with a
            % cross-section pgon using the floating Monte Carlo approximation.

            N = 1000;
            M = 50;

            % N random points within the polygon pgon

            points = duct.sample(pgon, N);

            % plot(pgon)
            % hold on
            % scatter(points(:, 1), points(:, 2), 'r*')

            tau = 1e-3;
            tic
            y = duct.floating(points, pgon, tau, M);
            toc

            keff = mean(y / 4 / M);

        end

        % ----- Tests ----- %

        function test1(a)
            % Compare the analytical permeability of a equilateral
            % triangular tube with the Monte Carlo results.

            if nargin < 1
                a = 1;
                disp('Using default side length a = 1')
            end

            k0 = a^2/80;
            pgon = duct.equilateral(a);
            kmc = duct.darcy(pgon);

            fprintf('kmc/k0 = %.2f\n', kmc / k0)

        end

        function test2(a, b)
            % Compare the analytical permeability of a rectangular tube
            % with the Monte Carlo results

            if nargin < 1
                a = 1;
                disp('Using default side length a = 1')
            end

            if nargin < 2
                b = a;
                disp('Square tube')
            end

            k0 = duct.rectangular(a, b);
            pgon = duct.rectangle(a, b);

            kmc = duct.darcy(pgon);
            fprintf('kmc/k0 = %.2f\n', kmc / k0)

        end

    end

end
