classdef frw
    % Approximate the permeability of synthetic soil particles using floating
    % random walk method.
    % The length unit for the particles is μm, and we do not scale the
    % length with a factor of 1e-6 because it only introduced a factor and will
    % be canceled in calculating the reduced permeability. However, appropriate
    % scaling is needed to obtain the dimensionless undercooling.

    methods (Static)

        % ----- Geometric operation ----- %

        function pzs = near(particles, z)
            % Find particles near the given height z.

            maxR = max(particles(:, 4));

            % in mono-dispersed case, one maxR may be not enough
            highid = particles(:, 3) < z + 2 * maxR;
            lowid = particles(:, 3) > z - 2 * maxR;
            pzs = particles(highid & lowid, :);
        end

        function Deff = diamter(particles)
            % Find the effective diameter using the sampled grain
            % volume and area.

            rs = particles(:, 4);
            Deff = 2 * sum(rs.^3) / sum(rs.^2);
        end

        function d = distance(p, zxy, zr)
            % Find minimum distance d on a plane from a point p = (x, y)
            % to multiple circles specified by their projected centers zxy and
            % radii zr.

            dists = vecnorm(zxy - p, 2, 2) - zr;

            if min(dists) < 0
                % inside one circle
                d = 0;
            else
                d = min(dists);
            end

        end

        % ----- RW calculation ----- %

        function points = sample(particles, z, N)
            % Generate N test points on the cut plane z.

            if nargin < 3
                N = 1000;

                if nargin < 2
                    z = mean(particles(:, 3));

                end

            end

            % N random points within a circle
            meanxy = mean(particles(:, 1:2));
            maxL = min(range(particles(:, 1:2))); % max side length

            % random points sampled in a circle
            rtest = sqrt(rand(N, 1)) * maxL * 0.4; % circle diameter is 80 %
            theta = 2 * pi * rand(N, 1);
            points = meanxy + rtest .* [cos(theta), sin(theta)];
            points(:, 3) = z;

            % sediment.xsection(particles, z)
            % hold on
            % scatter(points(:, 1), points(:, 2), 'r*')
            % axis equal

        end

        function y = floating(zxy, zr, pxy, tau, M)
            % Initiate floating random walk on a plane of normalized
            % circles specified by centers at zxy and radii zr, starting from
            % random points pxy, with a tolerance tau. The tallied result y may
            % contain zeros for points inside solid.

            close all
            % number of points
            N = size(pxy, 1);
            y = zeros(N, 1); % array of random walk tallies

            parfor i = 1:N

                p = pxy(i, :);

                rho = frw.distance(p, zxy, zr);

                if rho > 0

                    for j = 1:M
                        pn = p;
                        rho = frw.distance(pn, zxy, zr);
                        y(i) = y(i) + rho^2;

                        while rho > tau
                            theta = 2 * pi * rand;
                            pn = pn + rho * [cos(theta), sin(theta)];
                            rho = frw.distance(pn, zxy, zr);
                            y(i) = y(i) + rho^2;
                        end

                    end

                end

            end

        end

        function [phi, k] = permeability(y, M)
            % Calculate the porosity and effective permeability
            % from random walk tally.
            % The Darcy permeability must be multiplied by phi.

            N = numel(y);
            phi = nnz(y) / N;
            chi = rand(size(y));
            psi = rand(size(y)) * pi / 2;

            yp = nonzeros(y .* chi .* cos(psi)); % reduced random walk tallies
            % k = sum(yp) / N / 4 / M; % points in solid included
            k = geomean(yp) / 4 / M;
            % equivalent to geomean(nonzeros(y)) / 8 / M / exp(1)
        end

        function kref = kc(Deff, phi)
            % Calculate the Kozeny-Carman permeability.

            kref = phi^3/180 / (1 - phi)^2 * Deff^2;
        end

        function [keff, kref, phi] = darcy(particles, z, N)
            % Approximate the effective permeability for a random pack of
            % spherical particles using the floating random walk approximation,
            % with a reference Kozeny-Carman permeability kref provided for
            % comparison. The porosity is estimated using Monte Carlo method.

            % number of random walks
            M = 200;

            if nargin < 3

                % number of points
                N = 5000;

                if nargin < 2

                    z = mean(particles(:, 3));

                end

            end

            particles(:, 3) = particles(:, 3) - mean(particles(:, 3));

            meanR = mean(particles(:, 4));
            points = frw.sample(particles, z, N);
            pxy = points(:, 1:2);
            [zxy, zr] = sediment.project(particles, z);

            tau = 1e-3 * meanR;

            tic
            y = frw.floating(zxy, zr, pxy, tau, M);
            toc

            [phi, keff] = frw.permeability(y, M);
            keff = phi * keff;

            % Kozeny-Carman
            Deff = frw.diamter(frw.near(particles, z));
            kref = frw.kc(Deff, phi);

        end

        function keff = killed(particles, Sl, z, N)
            % Use killed random walk to account for the presence of ice
            % in the pore space. The liquid saturation Sl is interpreted as the
            % probability of a successful random walk step. If Sl = 1, the
            % original random walk scheme is recovered (but slower due to rand).
            % Film is taken into account.

            % number of random walks
            M = 200;

            if nargin < 4

                % number of points
                N = 5000;

                if nargin < 3

                    z = mean(particles(:, 3));

                end

            end

            particles(:, 3) = particles(:, 3) - mean(particles(:, 3));

            meanR = mean(particles(:, 4));
            points = frw.sample(particles, z, N);
            pxy = points(:, 1:2);
            [zxy, zr] = sediment.project(particles, z);

            tau = 1e-4 * meanR;
            y = zeros(N, 1);

            tic

            parfor i = 1:N

                p = pxy(i, :);

                rho = frw.distance(p, zxy, zr);

                if rho > 0

                    for j = 1:M
                        pn = p;
                        rnd = rand

                        if rnd > Sl
                            rnd = 0;
                        end

                        rho = frw.distance(pn, zxy, zr) * sqrt(rnd);

                        y(i) = y(i) + rho^2;

                        while rho > tau
                            theta = 2 * pi * rand;
                            pn = pn + rho * [cos(theta), sin(theta)];
                            rnd = rand

                            if rnd > Sl
                                rnd = 0;
                            end

                            rho = frw.distance(pn, zxy, zr) * sqrt(rnd);

                            y(i) = y(i) + rho^2;
                        end

                    end

                end

            end

            toc

            [phi, keff] = frw.permeability(y, M);
            keff = keff * phi * Sl;

        end

        function kr = icy(phi, Sl, dT, Slx, Deff)
            % Find the Kozeny-Carman permeability for partially frozen pores.
            % The effective diameter is in μm.

            if nargin < 5
                Deff = 2e-6; % default diameter is 2 micro
            end

            Slmin = 0.05;
            Slmax = 0.9;
            idx = Sl < Slmax & Sl > Slmin;
            p = polyfit(log(dT(idx)), log(Sl(idx)), 1);
            dTx = exp(log(Slx) / p(1) - p(2) / p(1));
            rc = 26.3e-9; % 0.0263 μm
            Dt = 4 * rc ./ dTx;

            phix = phi * Slx;
            rd = Dt .* (1 - phix) ./ (Dt * (1 - phi) ...
                + Deff * phi * (1 - Slx));

            kr = Slx.^3 * (1 - phi)^2 ./ (1 - phix).^2 .* rd.^2;

        end

        function k = reduction(particles, Sh)
            % Calcualte the permeability reduction relation with
            % varying Sh.

            if nargin < 2
                Sh = 0:0.05:0.9;
            end

            k = zeros(size(Sh));

            for i = 1:numel(Sh)
                k(i) = frw.killed(particles, 1 - Sh(i));
            end

        end

        function [Sh, kr] = permplot
            % Use empirical and semiempirical models to calculate the
            % permeability reduction with hydrate saturation, from Appendix A of
            % Lee (2008)

            Sh = 0:0.01:0.9;
            n = 1.5; % Archie exponent
            kr = zeros(6, numel(Sh));

            lc{1} = 'capillary coating';
            kr(1, :) = (1 - Sh).^2;
            lc{2} = 'capillary filling';
            kr(2, :) = 1 - Sh.^2 + 2 * (1 - Sh).^2 ./ log(Sh);
            lc{3} = 'grain coating';
            kr(3, :) = (1 - Sh).^(n + 1);
            lc{4} = 'pore filling';
            kr(4, :) = (1 - Sh).^(n + 2) ./ (1 + sqrt(Sh)).^2;
            lc{5} = 'University of Tokyo (M_S=10)';
            kr(5, :) = (1 - Sh).^10;
            lc{6} = 'LBNL';
            Sr = 0.09;
            m = 0.46;
            Sw = (1 - Sh - Sr) / (1 - Sr);
            kr(6, :) = sqrt(Sw) .* (1 - (1 - Sw.^(1 / m))).^2;

            % pack = 'data/deposit/Finney.mat';
            pack = 'data/deposit/cube-kts-0.3.mat';
            %
            particles = getfield(load(pack), 'particles');
            % kr = getfield(load('data/kr_0.3.mat'), 'kr');
            k = frw.reduction(particles, Sh);
            kr = [kr; k / k(1)];

            lc{end + 1} = 'floating random walk';

            figure
            cmap = lines(size(kr, 1));
            hold on

            for i = 1:size(kr, 1)
                plot(Sh, kr(i, :), 'linewidth', 2, 'color', cmap(i, :))
            end

            % modified Kozeny-Carman equation
            mcdata = 'data/mc/mc_finney_12_22_2020_16_37.txt';
            [T, Sl, phi] = soil.sfc(mcdata);
            kr_kc = frw.icy(phi, Sl.total, T.total, 1 - Sh);

            plot(Sh, kr_kc, 'k--', 'linewidth', 2)
            lc{end + 1} = 'modified Kozeny-Carman';

            % experimental measurements
            [k, S, G, ~, Gref] = frw.data;
            markers = ['+', 'o', '*', 'x', '^'];
            gscatter(S, k, G, 'b', markers, 8)
            lc = [lc, Gref'];

            ylim([1e-5, 1])
            xlim([0, 0.9])
            grid on
            box on
            set(gca, 'YScale', 'log')
            legend(lc, 'location', 'best')
            xlabel('S_h')
            ylabel('k_{r}')

            % save('kr_finney', 'Sh', 'kr')
            save('kr_0.3', 'Sh', 'kr')
        end

        function [kr, S, G, Glabel, Gref] = data
            % Read the experimental measurements from conductivity.xlsx.

            file = 'data/conductivity.xlsx';
            book = readtable(file, 'Sheet', 'kr_S', 'Range', 'A:C');

            % read data and phases
            kr = book.kr;
            S = book.S;
            G = book.Group;
            Glabel = 1:5;
            Gref = readcell(file, 'Sheet', 'kr_S', 'Range', 'G2:G6');
        end

        function manchester_dT
            % MANCHESTER_DT Calculate the conducvity change with undercooling
            % for the Manchester soil extracted from Horiguchi and Miller (1983).

            file = 'data/conductivity.xlsx';
            book = readtable(file, 'Sheet', 'Manchester_kH_dT', 'Range', 'A:C');
            kH = book.kH_m_s_;
            dT = book.dT_K_;
            G = book.Group;
            Gref = readcell(file, 'Sheet', 'Manchester_kH_dT', 'Range', 'G2:G3');
            % use the Manchester data only
            ind = ismember(G, find(contains(Gref, 'MANCHESTER')));

            % Manchester

            Deff = 6e-6;
            ratio = Deff / 2e-6;

            figure
            box on

            markers = ['^', 'o'];

            h = gscatter(dT(ind), kH(ind), G(ind), 'b', markers, 8);
            xlabel('dT (K)')
            ylabel('kH (m/s)')

            set(h, 'markerfacecolor', 'b')

            mcdata = 'data/mc/mc_finney_12_22_2020_16_37.txt';

            [dT, Sl, phi] = soil.sfc(mcdata, 1, ratio);
            % phi = 0.4;
            load('data/kr_finney.mat', 'Sh', 'kr')
            Slx = 1 - Sh(1:5:end);
            dTx = pchip(Sl.total, dT.total, Slx);

            kh0 = 3e-8;
            % param = jsondecode(fileread('param.json'));
            % g = 9.8;
            % rho = str2double(param.h2o.density.value);
            % mu = water.viscosity(273);
            % kh0 = frw.kc(Deff, phi) * rho * g / mu; % 6 times of chosen value
            fprintf('Ice-free kH: %.2e m/s\n', kh0);
            kx = kh0 * [1, kr(end, 1:5:end)];
            hold on
            plot([0, dTx], kx, 'r', 'LineWidth', 2)

            kr_kc = frw.icy(phi, Sl.total, dT.total, Slx, Deff);
            plot([0, dTx], kh0 * [1, kr_kc], 'k--', 'LineWidth', 2)

            set(gca, 'YScale', 'log')
            xlim([0, 0.14])

            lc = Gref(contains(Gref, 'MANCHESTER'));
            lc{end + 1} = 'floating random walk';
            lc{end + 1} = 'modified Kozeny-Carman';

            legend(lc, 'location', 'best')
        end

        function tottori_Sl
            % TOTTORI_SL Calculate the conducvity change with ice fraction for the
            % Tottori dune sand extracted from Watanabe and Wake (2009) and
            % Watanabe and Osada (2016).

            file = 'data/conductivity.xlsx';
            % read data and phases
            book = readtable(file, 'Sheet', 'Tottori_kH_Sl', 'Range', 'A:B');
            kH = book.kH_m_day_ / 86400;
            Sl = book.Sl;

            figure
            box on
            hold on

            semilogy(1 - Sl, kH, 'b*');
            xlabel('S')
            ylabel('kH (m/s)')
            set(gca, 'YScale', 'log')

            % Watanabe and Wake (2009) Table 1
            % Tottori dune is Sand2
            Cu = 1.7;
            sigma = log(Cu) / sqrt(2) / (erfcinv(0.2) - erfcinv(1.2));
            dm = 0.35e-3 / exp(sigma^2/2);
            ratio = dm * 1e6/2;

            Deff = dm * exp(2.5 * sigma^2);

            % mcdata = 'data/mc/mc_poly_0.30_06_27_2019_16_15.txt';
            mcdata = 'data/mc/mc_cube0.3_07_26_2019_17_11.txt';
            % mcdata = 'data/mc/mc_finney_12_22_2020_16_37.txt';
            [dT, Sl, phi] = soil.sfc(mcdata, 1, ratio);
            load('data/kr_finney.mat', 'Sh', 'kr')
            Slx = 1 - Sh;

            kh0 = 1.6e-5;
            % param = jsondecode(fileread('param.json'));
            % g = 9.8;
            % rho = str2double(param.h2o.density.value);
            % mu = water.viscosity(273);
            % kh0 = frw.kc(Deff, phi) * rho * g / mu; % 80 times chosen value
            fprintf('Ice-free kH: %.2e m/s\n', kh0);
            kx = kh0 * [1, kr(end, :)];
            hold on
            plot([0, Sh], kx, 'r', 'LineWidth', 2)

            kr_kc = frw.icy(phi, Sl.total, dT.total, Slx, Deff);
            plot([0, Sh], kh0 * [1, kr_kc], 'k--', 'LineWidth', 2)

            xlim([0.6, inf])
            lc = {'Tottori Sand (Watanabe and Osada, 2016)', 'floating random walk', ...
                'modified Kozeny-Carman'};
            legend(lc, 'location', 'best')
        end

    end

end
