classdef soil
    % This class does essentially the same thing as in class mc, but with
    % clearer IO. This should be used in favor of mc.m.

    methods (Static)

        % ----- MC geometry data ----- %

        function simulate(packfile, name, z, N)
            % Calculate the film thickness, and principal radii of ice
            % near N test points within cut plane z.

            particles = getfield(load(packfile), 'particles');

            if nargin < 4

                N = 5000;

                if nargin < 3
                    z = mean(particles(:, 3)); % default cut plane

                    if nargin < 2
                        name = 'finney';
                    end

                end

            end

            r = particles(:, 4);
            maxR = max(r);

            % in mono-dispersed case, one maxR gives wrong crevice
            highid = particles(:, 3) < z + 2 * maxR;
            lowid = particles(:, 3) > z - 2 * maxR;
            particles = particles(highid & lowid, :);

            fprintf('Number of test points: %d\n', N);

            points = frw.sample(particles, k, z, N);
            points(:, 3) = z;

            tstart = tic;
            [tf, rf, rp1, rp2, rp, rt] = soil.geometry(points, particles);

            % save results for future analysis
            timestamp = datestr(now, 'mm_dd_yyyy_HH_MM');
            telapsed = toc(tstart);
            fprintf('Elapsed time: %f seconds\n', telapsed);

            filename = ['mc_', name, '_', timestamp, '.txt'];

            % header info: sedment model name, number of test points, timestamp
            fid = fopen(filename, 'wt');
            fprintf(fid, 'Timestamp: %s\n', datestr(datenum(timestamp, ...
            'mm_dd_yyyy_HH_MM')));
            fprintf(fid, 'Run time: %f seconds\n', telapsed);
            fprintf(fid, 'Sediment model: %s\n', packfile);
            fprintf(fid, 'Test points: %d\n', N);
            fprintf(fid, 'pos(x, y, z), tf, rf, rp1, rp2, rp, rt\n');
            fprintf(fid, '---------------\n');
            fclose(fid);
            dlmwrite(filename, [points, tf, rf, rp1, rp2, rp, rt], ...
            '-append');
        end

        function [tf, rf, rp1, rp2, rp, rt] = geometry(points, particles)
            % Calculate the largest inscribed spheres near the test
            % points, and the distance d to the nearest particles. The length
            % unit is micrometer, the same as in particles.

            N = size(points, 1);
            tf = zeros(N, 1); % film thickness
            rf = zeros(N, 1); % film radius
            rp1 = zeros(N, 1); % first principal radius, positive
            rp2 = zeros(N, 1); % second principal radius, negative
            rp = zeros(N, 1); % pore radius
            rt = zeros(N, 1); % throat radius

            parfor i = 1:N

                ptest = points(i, :);

                [ids, dists] = grow.near(ptest, particles);
                pnear = particles(ids, :);

                if ~grow.cut(ptest, pnear)
                    % not within a particle
                    % film
                    rf(i) = pnear(1, 4); % radius of the nearest particle
                    tf(i) = dists(1);
                    % crevice
                    if grow.cone(ptest, pnear(1, :), pnear(2, :))
                        [ro, ri, ~] = grow.crevice(ptest, pnear(1:2, :));
                        rp1(i) = ro;
                        rp2(i) = ri;
                    end

                    % pore and throat
                    [sp, ispore] = grow.bulge(ptest, pnear);

                    if ispore
                        rp(i) = sp(4);
                    else
                        rt(i) = sp(4);
                    end

                end

            end

        end

        % ----- Undercoolings ----- %

        function [T, Sl, phi] = sfc(mcdata, zeta, scale)
            % Calculate the soil freezing curve from simulated results. The
            % undercooling of each type (film, crevice, pore + throat, and the
            % combined effect). The dimensionless undercooling has the same
            % value as the dimensional undercooling.

            if nargin < 2
                zeta = 1;
                scale = 1;
            end

            file = importdata(mcdata, ',', 6);
            data = file.data * scale;
            tf = data(:, 4);
            rf = data(:, 5);
            rp1 = data(:, 6);
            rp2 = data(:, 7);
            rp = data(:, 8) + data(:, 9);

            idf = tf > 0;
            idp = rp > 0;
            idc = rp1 > 0;
            N = size(data, 1);
            phi = sum(idf | idp | idc) / N;

            Tf = zeros(N, 1);
            Tp = zeros(N, 1); % temperature threshold for pore
            Tc = zeros(N, 1); % temperature threshold for crevice

            % because P0 Tm / (rhoi L) = 1 K, the dimensionless temperature has
            % the same value
            % film undercooling
            rc = 26.3e-3; % 0.0263 μm, gamma / P0
            lambda0 = 3.5e-3; % 0.0035 μm
            b = 3;
            Tf(idf) = (zeta * lambda0 ./ tf(idf)).^b - 2 * rc ./ rf(idf);
            Tf(Tf < 0) = 0;
            % crevice undercooling
            Tc(idc) = rc * (1 ./ rp1(idc) + 1 ./ rp2(idc));
            Tc(Tc < 0) = 0;
            % pore or throat undercooling
            Tp(idp) = 2 * rc ./ rp(idp);
            % combined effect
            Tliq = max([Tf, Tc, Tp], [], 2);
            % combined
            Ttotal = sort(Tliq(Tliq > 0), 'descend');
            Sltotal = (1:numel(Ttotal))' / numel(Ttotal);
            % film
            Tf = sort(Tf(Tf > 0), 'descend');
            Slf = (1:numel(Tf))' / numel(Ttotal);
            % crevice
            Tc = sort(Tc(Tc > 0), 'descend');
            Slc = (1:numel(Tc))' / numel(Ttotal);
            % pore and throat
            Tp = sort(Tp(Tp > 0), 'descend');
            Slp = (1:numel(Tp))' / numel(Ttotal);

            T = struct('film', Tf, 'crevice', Tc, 'pore', Tp, 'total', Ttotal);
            Sl = struct('film', Slf, 'crevice', Slc, 'pore', Slp, 'total', Sltotal);
        end

    end

end
